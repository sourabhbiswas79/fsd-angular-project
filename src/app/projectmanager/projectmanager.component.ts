
import {Component, OnInit} from '@angular/core';
@Component({
    selector: 'app-projectmanager',
    templateUrl:'./projectmanager.component.html',
    styles: [`
        .online{
            color: red;
        }
    `]


})
export class ProjectmanagerComponent implements OnInit{
projectCreationStatus = 'No project is created';
projectName='New Project';
    constructor(){

    }
ngOnInit(){

}

createProject(){
    this.projectCreationStatus='Project has been created'
    }

    onUpdateProjectName(event: Event){
this.projectName= (<HTMLInputElement>event.target).value;
    }

}
 