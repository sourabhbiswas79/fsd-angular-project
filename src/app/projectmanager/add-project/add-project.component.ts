import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms'
import { ObjectUnsubscribedError } from 'rxjs';
import { Options } from 'ng5-slider';
import { ProjectManagerSvc } from '../projectmanager.service';
import { IProject } from '../IProjectMaster';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { IUser } from '../IUserMaster';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddProjectComponent implements OnInit {
  addProjectForm: FormGroup;
  project: IProject;
  projects: IProject[];
  selectedUser: IUser;
  users: IUser[];
  user: IUser;
  button : string;
  searchString : string;

  validationMessages = {

    'projectName': {
      'required': 'Project name is required',
      'minlength': 'Project name must be greater than 3',
      'maxlength': 'Project name must be less than 20'
    },
    'startDate': {
      'required': 'Start Date is required',
      'dateValidation': 'Start date can not be greater than End date'
    },
    'endDate': {
      'required': 'End Date is required'
    },
    'priority': {
      'required': 'Priority is required'
    }
    ,
    'manager': {
      'required': 'Manager is required'
    }

  };
  formErrors = {
    'projectName': '',
    'startDate': '',
    'endDate': '',
    'priority': '',
    'manager': ''
  };
  btnClicked  = false;

  constructor(private http: HttpClient, private fb: FormBuilder, private projectmanagerservice: ProjectManagerSvc,
    private route: ActivatedRoute,
    private router: Router, private datePipe: DatePipe) { }
  // range: any = 0;
  // onRangeValueChange(event: any) {
  //   const value = event.value;
  //   this.range = value;

  // }

  value: number = 0;
  // highValue: number = 60;
  options: Options = {
    floor: 0,
    ceil: 100
  };
  orderByField: String;
  //   ngOnInit() {
  // this.addProjectForm = new FormGroup({
  // projectName: new FormControl() ,
  // manager: new FormControl()
  // });

  ngOnInit() {
    this.addProjectForm = this.fb.group({
      projectName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      manager: ['',Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      priority: [''],
      value: '0',
      userInfo: ['']

    });
    //this.addProjectForm.get('manager').disable();
    //this.formItemKeyValue(this.addProjectForm);
     this.addProjectForm.valueChanges.subscribe((data) => {
      this.validationErrors(this.addProjectForm);
      });
   this.button = 'Add';
    this.route.paramMap.subscribe(params => {
      const projectId = +params.get('projectId');
      if (projectId) {
        this.getProject(projectId);
      } else {

        this.project = {
          projectId: null,
          projectName: '',
          startDate: new Date,
          endDate: new Date,
          priority: 0,
          completeStatus: '',
          totalTask: 0,
          manager: ''
        };
      }
    });


    this.projectmanagerservice.getProjects().subscribe(
      (listProject) => this.projects = listProject,
      (err) => console.log(err)
    );

    this.user = {
      userId: '',
      firstName: '',
      lastName: '',
      empId: null,
      projectId: null,
      taskId: null
    };

    //  let obj= this.http.get('http://localhost:8080/restservices/getProjectDetails?projectName=Test')
    // obj.subscribe((response) =>console.log(response));

    // this.addProjectForm.setValue({
    //   projectName: obj.subscribe((response) => (JSON.stringify(response)).toString),
    //   manager: obj.subscribe((response) => JSON.stringify(response))
    // });

    this.projectmanagerservice.getUsers().subscribe(
      (listUser) => this.users = listUser,
      (err) => console.log(err)
    );


  }




  getProject(id: number) {
    this.projectmanagerservice.getProject(id).subscribe(

      (project: IProject) => {
        this.addProjectForm.clearAsyncValidators;
        this.editProject(project);
        this.project = project;
      },
      (err: any) => console.log(err)
    );
  }

  editProject(project: IProject) {
    
    this.addProjectForm.patchValue({
      projectId: project.projectId,
      projectName: project.projectName,
      startDate: this.datePipe.transform(new Date(project.startDate), 'yyyy-MM-dd'),
      endDate: this.datePipe.transform(new Date(project.endDate), 'yyyy-MM-dd'),
      manager: project.manager
    });
    this.value = project.priority;
    this.button='Update';
    //console.log(this.datePipe.transform(new Date(project.startDate), 'MM/dd/yyyy'));
    // this.addProjectForm.get('startDate').setValue(new Date(project.startDate));
    //this.addProjectForm.get('startDate').setValue(this.datePipe.transform(new Date(project.startDate), 'yyyy-MM-dd'));
  }
  createProject() {
    this.btnClicked = true;
    //this.addProjectForm.valueChanges.subscribe((data) => {
    // this.validationErrors(this.addProjectForm);
    //});

    this.validationErrors(this.addProjectForm);

    if (null != this.addProjectForm.get('startDate').value && null != this.addProjectForm.get('endDate').value) {

      if (new Date(this.addProjectForm.get('startDate').value) > new Date(this.addProjectForm.get('endDate').value)) {

        this.formErrors.endDate = 'End date can not be less than Start date';
      }

    }

    if (this.addProjectForm.invalid) {
      return;
    }

    this.mapFormValuesForProjectDetails();
    if (this.project.projectId) {
      this.projectmanagerservice.updateProject(this.project)
        .subscribe(
          (err: any) => console.log(err)
        );
    } else {

      this.projectmanagerservice.addProject(this.project)
        .subscribe(
          () => this.router.navigateByUrl('addProject'),
          (err: any) => console.log(err)
        );

    }
    window.location.reload();

  }


  mapFormValuesForProjectDetails() {
    if (this.project.projectId) {
      console.log("project id is:" + this.project.projectId)
    } else {
      this.project.projectId = null;
    }
    this.project.projectName = this.addProjectForm.get('projectName').value;
    this.project.startDate = this.addProjectForm.get('startDate').value;
    this.project.endDate = this.addProjectForm.get('endDate').value;
    this.project.priority = this.value;
    this.project.completeStatus = 'N';
    this.project.manager = this.addProjectForm.get('manager').value;
  }

  resetForm() {
    this.clearForm(this.addProjectForm);
  }


  closeProject(id: number) {
    this.projectmanagerservice.getProject(id).subscribe(

      (project: IProject) => {
        this.updateProject(project);
        this.project = project;
      },
      (err: any) => console.log(err)
    );
  }

  updateProject(project: IProject) {
    this.project.projectId = project.projectId;
    this.project.projectName = project.projectName;
    this.project.endDate = project.endDate;
    this.project.startDate = project.startDate;
    this.project.priority = project.priority;
    this.project.manager = project.manager;
    this.project.completeStatus = 'Y';
    this.projectmanagerservice.updateProject(this.project)
      .subscribe(
        (err: any) => console.log(err)
      );

    window.location.reload();
  }

  clearForm(group: FormGroup) {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.clearForm(abstractControl);
      } else {
        abstractControl.reset();
      }
    });
    this.button='Add';
    this.addProjectForm.reset;
    this.value =0;
    this.formErrors = {
      'projectName': '',
      'startDate': '',
      'endDate': '',
      'priority': '',
      'manager': ''
    };
  }


  validationErrors(group: FormGroup = this.addProjectForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstarctControl = group.get(key);
      if (abstarctControl instanceof FormGroup) {
        this.validationErrors(abstarctControl);
        console.log('instance of formgroup');
      } else {
        this.formErrors[key] = ' ';
        if (abstarctControl && !abstarctControl.valid) {
          const msg = this.validationMessages[key];
          console.log(msg);
          // console.log(abstarctControl.errors);
          for (const errKey in abstarctControl.errors) {
            if (errKey) {
              this.formErrors[key] += msg[errKey] + ' ';
            }
          }
        }
      }
    });
  }




  setDate = true;
  toggle() {
    this.setDate = !this.setDate;
    console.log(this.setDate)
    // if (!this.setDate) {

    //   document.getElementById("startDate").setAttribute('disabled', 'true');
    //   document.getElementById("endDate").setAttribute('disabled', 'true');

    // } else {

    //   document.getElementById("startDate").removeAttribute('disabled');
    //   document.getElementById("endDate").removeAttribute('disabled');

    // }
    this.formItemKeyValue(this.addProjectForm);
    
  }
  public userModalIsOpen: boolean = false;
  public openUserModal(open: boolean): void {
    this.userModalIsOpen = open;
  }

  closeModalDialog() {
    this.userModalIsOpen = false;
  }

  onSelectUser(user: IUser) {
    this.selectedUser = user;
    this.userModalIsOpen = false;
    this.addProjectForm.patchValue({
      manager: user.firstName + ' ' + user.lastName
    });

  }

  sortByStartDate() {

    this.projects.sort(function (a, b) {
      return a.startDate > b.startDate ? -1 : 1
    });
  }

  sortByEndDate() {

    this.projects.sort(function (a, b) {
      return a.endDate > b.endDate ? -1 : 1
    });
  }

  sortByPriority() {

    this.projects.sort(function (a, b) {
      return a.priority > b.priority ? -1 : 1
    });
  }

  sortByCompleted() {

    this.projects.sort(function (a, b) {
      return a.completeStatus > b.completeStatus ? 1 : -1
    });
  }

formItemKeyValue(group : FormGroup): void{
  Object.keys(group.controls).forEach((key: string) => {
    const abstarctControl = group.get(key);
    if (abstarctControl instanceof FormGroup) {
      this.validationErrors(abstarctControl);
      console.log('instance of formgroup');
    } else {
      if(!this.setDate){
        if(key == 'startDate' || key =='endDate'){
          console.log('*************'+key)
          abstarctControl.disable();
        }
      }else{
        if(key == 'startDate' || key =='endDate'){
          abstarctControl.enable();
        }
      }
      // if (key == 'manager'){
      //   abstarctControl.disable();
      // }
    }
  });

}

}
