export interface IUser{
    userId:string;
    firstName:string;
    lastName:string;
    empId:number;
    projectId: number;
    taskId:number;

}