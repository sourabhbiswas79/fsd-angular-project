export interface ITask{
    taskId: number;
    projectId: number;
    parentId: number;
    taskName: string;
    startDate: Date;
    endDate: Date;
    priority:number;
    status: string;
}

    