import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule,Routes,Navigation} from '@angular/router'
import { AddProjectComponent } from '../add-project/add-project.component';
import { AddTaskComponent } from '../add-task/add-task.component';
import { AddUserComponent } from '../add-user/add-user.component';
import { ViewTaskComponent } from '../view-task/view-task.component';

const appRoutes: Routes =  [
{path: 'addProject', component: AddProjectComponent},
{path: 'addTask', component: AddTaskComponent},
{path: 'editTask/:taskId', component: AddTaskComponent},
{path: 'addUser', component: AddUserComponent},
{path: 'viewTask', component: ViewTaskComponent},
{path: '',redirectTo: '/appProject',pathMatch: 'full'}
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes,{enableTracing: true})
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
