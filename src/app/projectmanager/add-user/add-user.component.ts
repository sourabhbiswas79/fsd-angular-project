import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormGroup,FormControl,Validators,FormBuilder} from '@angular/forms'
import { ObjectUnsubscribedError } from 'rxjs';
import {ProjectManagerSvc} from '../projectmanager.service';
import { IUser } from '../IUserMaster';
import {ActivatedRoute,Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import {Validation} from '../projectmanager.validation';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  user: IUser;
  users: IUser[];
  btnClicked = 'false';
  validationMessages = {

    'firstName': {
      'required': 'First name is required'
    },
    'lastName': {
      'required': 'Last name  is required'
    },
    'empId': {
      'required': 'Employee Id is required'
    }

  };
  formErrors = {
    'firstName': '',
    'lastName': '',
    'empId': ''
  };

  constructor(private http: HttpClient,private fb: FormBuilder,
    private projectmanagerservice: ProjectManagerSvc,
    private route : ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.addUserForm = this.fb.group({
      firstName: ['',Validators.required],
      lastName: ['',Validators.required],
      empId: [null,Validators.required],
      userId: [''],
      projectId: 0,
      taskId: 0
    });

this.route.paramMap.subscribe(params =>{
  const userId = +params.get('userId');
  if(userId){
    this.getUser(userId);
  }else{
    this.user = {
      userId: '',
      firstName : '',
      lastName : '',
      empId : null,
      projectId :null,
      taskId: null
    };
  }
});

this.projectmanagerservice.getUsers().subscribe(
  (listUser) => this.users = listUser,
  (err) => console.log(err)
  );

  }

  getUser(userId: number){
    this.projectmanagerservice.getUser(userId).subscribe(
  
      (user: IUser) =>{
        this.user=user;
        this.editUser(user);
       
      },
      (err: any) =>console.log(err)
    );
  }

  deleteUser(userId: number){
    this.projectmanagerservice.deleteUser(userId).subscribe(
      (err: any) => console.log(err));
      window.location.reload();
  }

editUser(user: IUser){
  this.addUserForm.patchValue({
    userId: user.userId,
    firstName: user.firstName,
    lastName: user.lastName,
    empId: user.empId
  });
}

createUser(){
  this.btnClicked = 'true';
  this.validationErrors(this.addUserForm);
  if(this.addUserForm.invalid){
    return;
  }
  this.mapFormValuesForUserDetails();
  if(this.user.userId){
  this.projectmanagerservice.updateUser(this.user)
  .subscribe(
    (err: any) => console.log(err)
  );
  }else {
    
    this.projectmanagerservice.addUser(this.user)
    .subscribe(
      () => this.router.navigateByUrl('addUser'),
      (err:any) => console.log(err)
    );
    
    
  }
  window.location.reload();

  }


  mapFormValuesForUserDetails(){
    if(this.user.userId){
   console.log("User id is:" +this.user.userId)
    }else{
      this.user.userId = null;
    }
    this.user.firstName = this.addUserForm.get('firstName').value;
    this.user.lastName = this.addUserForm.get('lastName').value;
    this.user.empId = this.addUserForm.get('empId').value;
    
  }

  resetForm(){
    this.clearForm(this.addUserForm);
  }

clearForm(group: FormGroup){

Object.keys(group.controls).forEach((key: string) => {
const abstractControl = group.get(key);
if (abstractControl instanceof FormGroup){
this.clearForm(abstractControl);
}else {
abstractControl.reset();
}
});
this.formErrors = {
  'firstName': '',
  'lastName': '',
  'empId': ''
};
}

validationErrors(group: FormGroup = this.addUserForm): void {
  Object.keys(group.controls).forEach((key: string) => {
    const abstarctControl = group.get(key);
    if (abstarctControl instanceof FormGroup) {
      this.validationErrors(abstarctControl);
      console.log('instance of formgroup');
    } else {
      this.formErrors[key] = ' ';
      if (abstarctControl && !abstarctControl.valid) {
        const msg = this.validationMessages[key];
        console.log(msg);
        // console.log(abstarctControl.errors);
        for (const errKey in abstarctControl.errors) {
          if (errKey) {
            this.formErrors[key] += msg[errKey] + ' ';
          }
        }
      }
    }
  });
}
}
