import { Injectable } from '@angular/core';
import {IProject} from './IProjectMaster'
import { ITask } from './ITaskMaster';
import { IUser } from './IUserMaster';
import {IParentTask} from './IParentTask';
import {Observable,throwError} from 'rxjs'
import {catchError} from 'rxjs/operators'
import {HttpClient,HttpErrorResponse,HttpHeaders} from '@angular/common/http'
@Injectable()
export class ProjectManagerSvc {


    constructor(private httpClient: HttpClient){}


    baseUrl ='http://localhost:8082/restservices';

    getProjectDetails(): Observable<IProject[]>{
    return this.httpClient.get<IProject[]>(this.baseUrl)
    .pipe(catchError(this.handleError))

    }
    
    private handleError(errorResponse : HttpErrorResponse){
        if(errorResponse.error instanceof ErrorEvent){
            console.error('Client side error',errorResponse.error);

        }else {
            console.error('Server side erroor',errorResponse.error);
        }
        return throwError('Problem processing the request');
    }


addProject(project : IProject): Observable<IProject>{
    console.log(project.projectName);
    return this.httpClient.post<IProject>(`${this.baseUrl}/addProject`,project,{
        headers: new HttpHeaders({
'Content-Type' :'application/json'
        })
    })
    .pipe(catchError(this.handleError));


}


getProjects(): Observable<IProject[]>{
    return this.httpClient.get<IProject[]>(`${this.baseUrl}/getProjectDetails`)
    .pipe(catchError(this.handleError));
}

getProject(projectId: number): Observable<IProject>{
    return this.httpClient.get<IProject>(`${this.baseUrl}/getProject?projectId=${projectId}`
)
    .pipe(catchError(this.handleError));
}

updateProject(project : IProject): Observable<void>{
return this.httpClient.put<void>(`${this.baseUrl}/updateProject`,project,

{
    headers: new HttpHeaders({
'Content-Type' :'application/json'
    })
}).pipe(catchError(this.handleError));
}

deleteProject(projectId: number): Observable<void>{
    return this.httpClient.delete<void>(`${this.baseUrl}/${projectId}`)
    .pipe(catchError(this.handleError));
}

addTask(task : ITask): Observable<ITask>{
    console.log(task.taskName);
    return this.httpClient.post<ITask>(`${this.baseUrl}/addTask`,task,{
        headers: new HttpHeaders({
'Content-Type' :'application/json'
        })
    })
    .pipe(catchError(this.handleError));
}

addParentTask(parentTask : IParentTask): Observable<IParentTask>{
    console.log(parentTask.parentTaskName);
    return this.httpClient.post<IParentTask>(`${this.baseUrl}/addParentTask`,parentTask,{
        headers: new HttpHeaders({
'Content-Type' :'application/json'
        })
    })
    .pipe(catchError(this.handleError));
}

getParentTasks(): Observable<IParentTask[]>{
    return this.httpClient.get<IParentTask[]>(`${this.baseUrl}/getParentTasks`)
    .pipe(catchError(this.handleError));
}


getParentTask(parentId: number): Observable<IParentTask>{
    return this.httpClient.get<IParentTask>(`${this.baseUrl}/getParentTask?parentId=${parentId}`)
    .pipe(catchError(this.handleError));
}


getParentName(parentId :number): Observable<string>{
    return this.httpClient.get<string>(`${this.baseUrl}/getParentName?parentId=${parentId}`
)
    .pipe(catchError(this.handleError));
}

updateTask(task: ITask): Observable<void>{
    return this.httpClient.put<void>(`${this.baseUrl}/updateTask`,task,   
    {
        headers: new HttpHeaders({
    'Content-Type' :'application/json'
        })
    }).pipe(catchError(this.handleError));
    }

    getTasks(projectId: number): Observable<ITask[]>{
        return this.httpClient.get<ITask[]>(`${this.baseUrl}/getTasks?projectId=${projectId}`
    )
        .pipe(catchError(this.handleError));
    }

    getTask(taskId: number): Observable<ITask>{
        return this.httpClient.get<ITask>(`${this.baseUrl}/getTask?taskId=${taskId}`
    )
        .pipe(catchError(this.handleError));
    }

    addUser(user : IUser): Observable<IUser>{
        console.log(user.firstName);
        return this.httpClient.post<IUser>(`${this.baseUrl}/addUser`,user,{
            headers: new HttpHeaders({
    'Content-Type' :'application/json'
            })
        })
        .pipe(catchError(this.handleError));
    
    
    }
    
    
    getUsers(): Observable<IUser[]>{
        return this.httpClient.get<IUser[]>(`${this.baseUrl}/getUserDetails`)
        .pipe(catchError(this.handleError));
    }
    
    getUser(userId: number): Observable<IUser>{
        return this.httpClient.get<IUser>(`${this.baseUrl}/getUser?userId=${userId}`
    )
        .pipe(catchError(this.handleError));
    }
    
    getUserForProject(projectId: number): Observable<IUser>{
        return this.httpClient.get<IUser>(`${this.baseUrl}/getUserForProject?projectId=${projectId}`
    )
        .pipe(catchError(this.handleError));
    }

    updateUser(project : IUser): Observable<void>{
    return this.httpClient.put<void>(`${this.baseUrl}/updateUser`,project,
    
    {
        headers: new HttpHeaders({
    'Content-Type' :'application/json'
        })
    }).pipe(catchError(this.handleError));
    }
    
    deleteUser(userId: number): Observable<void>{
        return this.httpClient.delete<void>(`${this.baseUrl}/deleteUser?userId=${userId}`)
        .pipe(catchError(this.handleError));
    }
      

ngOnInit(){

}
}