import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'
import { ObjectUnsubscribedError } from 'rxjs';
import { Options } from 'ng5-slider';
import { ProjectManagerSvc } from '../projectmanager.service';
import { IProject } from '../IProjectMaster';
import { ITask } from '../ITaskMaster';
import { IUser } from '../IUserMaster';
import { IParentTask } from '../IParentTask';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FilterPipe } from '../projectmanager.filter-pipe';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  addTaskForm: FormGroup;
  project: IProject;
  task: ITask;
  parentTask: IParentTask;
  projects: IProject[];
  selectedProject: IProject;
  selectedTask: ITask;
  selectedUser: IUser;
  selectedParentTask: IParentTask;
  parentTasks: IParentTask[];
  users: IUser[];
  user: IUser;
  public searchProject: string;
  public searchParent: string;
  public parentTaskName: string;
  public projectName: string;
  display = 'none';
  btnClicked = 'false';
  button: string;
  searchString : string;
  validationMessages = {

    'projectName': {
      'required': 'Project name is required'
    },
    'startDate': {
      'required': 'Start Date is required',
      'dateValidation': 'Start date can not be greater than End date'
    },
    'endDate': {
      'required': 'End Date is required'
    },
    'taskName': {
      'required': 'Task Name is required'
    }
    ,
    'parentTaskName': {
      'required': 'Parent TaskName is required'
    },
    'userInfo': {
      'required': 'User is required'
    }

  };
  formErrors = {
    'projectName': '',
    'startDate': '',
    'endDate': '',
    'taskName': '',
    'parentTaskName': '',
    'userInfo': ''
  };


  constructor(private http: HttpClient,
    private fb: FormBuilder,
    private projectmanagerservice: ProjectManagerSvc,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private filterPipe: FilterPipe) { }

  values: number = 0;
  option: Options = {
    floor: 0,
    ceil: 100
  };

  ngOnInit() {

    this.addTaskForm = this.fb.group({
      projectName: ['', Validators.required],
      taskName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      priority: [''],
      parentTaskName: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      userInfo: ['', Validators.required],

      values: '0'
    });

    this.addTaskForm.valueChanges.subscribe((data) => {
      this.validationErrors(this.addTaskForm);
    });
    this.button = 'Add';


    this.route.paramMap.subscribe(params => {
      const taskId = +params.get('taskId');
      if (taskId) {
        this.getTask(taskId);
        this.projectName = '';
      } else {

        this.task = {
          projectId: null,
          parentId: null,
          taskId: null,
          taskName: '',
          startDate: new Date,
          endDate: new Date,
          priority: 0,
          status: ''
        };
      }
      this.project = {
        projectId: null,
        projectName: '',
        startDate: new Date,
        endDate: new Date,
        priority: 0,
        completeStatus: '',
        totalTask: 0,
        manager: ''
      };

    });



    this.projectmanagerservice.getProjects().subscribe(
      (listProject) => this.projects = listProject,
      (err) => console.log(err)
    );

    this.projectmanagerservice.getParentTasks().subscribe(
      (listParentTask) => this.parentTasks = listParentTask,
      (err) => console.log(err)
    );
    this.projectmanagerservice.getUsers().subscribe(
      (listUser) => this.users = listUser,
      (err) => console.log(err)
    );

    this.task = {
      projectId: null,
      taskId: null,
      parentId: null,
      taskName: '',
      startDate: new Date,
      endDate: new Date,
      priority: 0,
      status: ''
    };
    this.parentTask = {
      parentId: null,
      parentTaskName: ''
    };
    this.selectedParentTask = {
      parentId: null,
      parentTaskName: ''
    };
    this.user = {
      userId: '',
      firstName: '',
      lastName: '',
      empId: null,
      projectId: null,
      taskId: null
    };

    this.selectedProject = {
      projectId: null,
      projectName: '',
      startDate: new Date,
      endDate: new Date,
      priority: 0,
      completeStatus: '',
      totalTask: 0,
      manager: ''
    };


  }

  getTask(id: number) {
    this.projectmanagerservice.getTask(id).subscribe(

      (task: ITask) => {
        this.editTask(task);
        this.task = task;
      },
      (err: any) => console.log(err)
    );
  }

  editTask(task: ITask) {
    this.button = 'Update';
    this.projectmanagerservice.getProject(task.projectId).subscribe(
      (project: IProject) => {
        this.project = project;
        this.addTaskForm.patchValue({ projectName: project.projectName });
        this.selectedProject.projectId = project.projectId;
      },
      (err: any) => console.log(err)
    );

    this.projectmanagerservice.getParentTask(task.parentId).subscribe(
      (parentTask: IParentTask) => {
        this.parentTask = parentTask;
        this.addTaskForm.patchValue({ parentTaskName: parentTask.parentTaskName });
        this.selectedParentTask.parentId = parentTask.parentId;
      },
      (err: any) => console.log(err)
    );

    this.projectmanagerservice.getUserForProject(task.projectId).subscribe(
      (user: IUser) => {

        this.addTaskForm.patchValue({ userInfo: user.firstName + ' ' + user.lastName });

      },
      (err: any) => console.log(err)
    );
    this.addTaskForm.patchValue({
      projectId: task.projectId,
      // projectName: this.project.projectName,
      taskName: task.taskName,
      //parentTaskName:  this.parentTask.parentTaskName,
      startDate: this.datePipe.transform(new Date(task.startDate), 'yyyy-MM-dd'),
      endDate: this.datePipe.transform(new Date(task.endDate), 'yyyy-MM-dd')

    });
    this.values = task.priority;
    //console.log(this.datePipe.transform(new Date(project.startDate), 'MM/dd/yyyy'));
    // this.addProjectForm.get('startDate').setValue(new Date(project.startDate));
    //this.addProjectForm.get('startDate').setValue(this.datePipe.transform(new Date(project.startDate), 'yyyy-MM-dd'));
  }


  isChecked = false;
  toggle() {
    this.isChecked = !this.isChecked;
    console.log(this.isChecked)
    if (this.isChecked) {
      document.getElementById("priority").setAttribute('disabled', 'true');
      this.option = {
        floor: 0,
        ceil: 0
      };
      document.getElementById("parentTaskBtn").setAttribute('disabled', 'true');
      document.getElementById("startDate").setAttribute('disabled', 'true');
      document.getElementById("endDate").setAttribute('disabled', 'true');
      document.getElementById("userButton").setAttribute('disabled', 'true');
      this.formErrors.startDate='';
      this.formErrors.endDate='';
      this.formErrors.userInfo='';
      this.formErrors.parentTaskName='';
      
    } else {
      document.getElementById("priority").removeAttribute('disabled');
      this.option = {
        floor: 0,
        ceil: 100
      };
      document.getElementById("parentTaskBtn").removeAttribute('disabled');
      document.getElementById("startDate").removeAttribute('disabled');
      document.getElementById("endDate").removeAttribute('disabled');
      document.getElementById("userButton").removeAttribute('disabled');
    }

  }

  public projectModalIsOpen: boolean = false;
  public openProjectModal(open: boolean): void {
    this.projectModalIsOpen = open;
  }
  public taskModalIsOpen: boolean = false;
  public openTaskModal(open: boolean): void {
    this.taskModalIsOpen = open;
  }
  public userModalIsOpen: boolean = false;
  public openUserModal(open: boolean): void {
    this.userModalIsOpen = open;
  }

  closeModalDialog() {
    this.projectModalIsOpen = false;
    this.taskModalIsOpen = false;
    this.userModalIsOpen = false;
  }
  onSelectProject(project: IProject) {
    this.selectedProject = project;
    this.projectModalIsOpen = false;
    this.addTaskForm.patchValue({
      projectName: project.projectName,
      projectId: project.projectId

    });
  }
  onSelectParentTask(parentTask: IParentTask) {
    this.selectedParentTask = parentTask;
    this.taskModalIsOpen = false;
    this.addTaskForm.patchValue({
      parentTaskName: parentTask.parentTaskName,
      parentId: parentTask.parentId

    });
    console.log(parentTask.parentId);
  }

  onSelectUser(user: IUser) {
    this.selectedUser = user;
    this.userModalIsOpen = false;
    this.addTaskForm.patchValue({
      userInfo: user.firstName + ' ' + user.lastName
    });

  }

  createTask() {

    
    this.validationErrors(this.addTaskForm);
    this.btnClicked = 'true';
   
    if(this.isChecked){
      this.formErrors.startDate='';
      this.formErrors.endDate='';
      this.formErrors.userInfo='';
      this.formErrors.parentTaskName='';
    }
    if (this.addTaskForm.invalid) {
      return;
    }
    //console.log(this.addProjectForm.value)
    this.mapFormValuesForTaskDetails();
    // if(this.selectedProject.projectId ){
    // this.projectmanagerservice.updateTask(this.task)
    // .subscribe(
    //   (err: any) => console.log(err)
    // );
    // }else {
    if (this.isChecked) {
      console.log("inside add parent task");
      this.projectmanagerservice.addParentTask(this.parentTask)
        .subscribe(
          (err1: any) => console.log(err1)
        );
    } else {
      this.projectmanagerservice.addTask(this.task)
        .subscribe(
          () => this.router.navigateByUrl('addTask'),
          (err: any) => console.log(err)
        );
      this.projectmanagerservice.updateUser(this.selectedUser)
        .subscribe(
          () => this.router.navigateByUrl('addTask'),
          (err: any) => console.log(err)
        );
    }

    // }
    window.location.reload();

  }


  mapFormValuesForTaskDetails() {
    console.log(this.isChecked);
    if (!this.isChecked) {
      console.log('inside loop');
      if (this.selectedProject.projectId && this.selectedParentTask.parentId) {
        console.log("project id is:" + this.selectedProject.projectId);
        console.log("Parent task id is:" + this.selectedParentTask.parentId);
        this.task.parentId = this.selectedParentTask.parentId;
      } else {
        this.task.taskId = null;
        this.task.parentId = null;
      }

      this.task.projectId = this.selectedProject.projectId;
      //this.task.parentId = this.selectedParentTask.parentId;
      // this.task.taskId = this.addTaskForm.get('parentId').value;
      this.task.taskName = this.addTaskForm.get('taskName').value;
      this.task.startDate = this.addTaskForm.get('startDate').value;
      this.task.endDate = this.addTaskForm.get('endDate').value;
      this.task.priority = this.values;
      this.task.status = 'ACTIVE';

      this.selectedUser.projectId = this.selectedProject.projectId;

    }
    else {
      this.parentTask.parentId = null;
      this.parentTask.parentTaskName = this.addTaskForm.get('taskName').value;
      //this.parentTask.parentId = null;
    }

  }
  resetForm() {
    this.clearForm(this.addTaskForm);
  }
  clearForm(group: FormGroup) {
    
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.clearForm(abstractControl);
      } else {
        abstractControl.reset();
      }
    });
    this.formErrors = {
      'projectName': '',
      'startDate': '',
      'endDate': '',
      'taskName': '',
      'parentTaskName': '',
      'userInfo': ''
    };
  }

  validationErrors(group: FormGroup = this.addTaskForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstarctControl = group.get(key);
      if (abstarctControl instanceof FormGroup) {
        this.validationErrors(abstarctControl);
        console.log('instance of formgroup');
      } else {
        this.formErrors[key] = ' ';
        if (abstarctControl && !abstarctControl.valid) {
          const msg = this.validationMessages[key];
          console.log(msg);
          // console.log(abstarctControl.errors);
          for (const errKey in abstarctControl.errors) {
            if (errKey) {
              this.formErrors[key] += msg[errKey] + ' ';
            }
          }
        }
      }
    });
  }


}
