export interface IProject{
    projectId: number;
    projectName: string;
    startDate: Date;
    endDate: Date;
    priority:number;
    completeStatus: string;
    totalTask: number;
    manager: string;
}

    