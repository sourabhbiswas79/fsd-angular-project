import {Component} from '@angular/core' 
import {FormControl,FormGroup,} from '@angular/forms'

export class Validation{

    formGrp : FormGroup;
    validationMessages = {

        'firstName': {
          'required': 'First name is required'
        },
        'lastName': {
          'required': 'Last name  is required'
        },
        'empId': {
          'required': 'Employee Id is required'
        }
    
      };
      formErrors = {
        'firstName': '',
        'lastName': '',
        'empId': ''
      };

    validationErrors(group: FormGroup = this.formGrp): void {
        Object.keys(group.controls).forEach((key: string) => {
          const abstarctControl = group.get(key);
          if (abstarctControl instanceof FormGroup) {
            this.validationErrors(abstarctControl);
            console.log('instance of formgroup');
          } else {
            this.formErrors[key] = ' ';
            if (abstarctControl && !abstarctControl.valid) {
              const msg = this.validationMessages[key];
              console.log(msg);
              // console.log(abstarctControl.errors);
              for (const errKey in abstarctControl.errors) {
                if (errKey) {
                  this.formErrors[key] += msg[errKey] + ' ';
                }
              }
            }
          }
        });
      }

      validatteForm(frm: FormGroup){
          this.formGrp = frm;
          this.validationErrors(this.formGrp);

      }
}