import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormGroup,FormControl,Validators,FormBuilder} from '@angular/forms'
import { ObjectUnsubscribedError } from 'rxjs';
import { Options } from 'ng5-slider';
import {ProjectManagerSvc} from '../projectmanager.service';
import { IProject } from '../IProjectMaster';
import { ITask } from '../ITaskMaster';
import { IUser } from '../IUserMaster';
import {IParentTask} from '../IParentTask';
import {ActivatedRoute,Router} from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {
  viewTaskForm: FormGroup;
  project: IProject;
  task: ITask;
  parentTask: IParentTask;
  projects: IProject[];
  selectedProject: IProject;
  selectedTask: ITask;
  selectedUser: IUser;
  selectedParentTask: IParentTask;
  parentTasks: IParentTask[];
  users: IUser[];
  user: IUser;
  tasks: ITask[];
  public searchProject: string;
  public searchParent: string;
  display = 'none';



  constructor(private http: HttpClient,
    private fb: FormBuilder,
    private projectmanagerservice: ProjectManagerSvc,
    private route : ActivatedRoute,
    private router: Router) { }

  ngOnInit() {

    this.viewTaskForm = this.fb.group({
      projectName: ['']
      });
    this.projectmanagerservice.getProjects().subscribe(
      (listProject) => this.projects = listProject,
      (err) => console.log(err)
      );

  }

  public projectModalIsOpen : boolean=false;
  public openProjectModal(open : boolean) : void{
    this.projectModalIsOpen = open;
  }

  closeModalDialog(){
    this.projectModalIsOpen = false;

   }  

   onSelectProject(project: IProject){
    this.selectedProject=project;
    this.projectModalIsOpen = false;
    this.viewTaskForm.patchValue({
      projectName: project.projectName,
      projectId: project.projectId
    
    });

    this.projectmanagerservice.getTasks(project.projectId).subscribe(
      (listTask) => this.tasks = listTask,
      (err) => console.log(err)
      );


    }

    getTask(taskId : number){
      this.router.navigate(['/editTask',taskId]);
    }
    
    closeTask(taskId : number){
      this.projectmanagerservice.getTask(taskId).subscribe(
  
        (task: ITask) =>{
          this.task=task;
          this.task.status = 'Closed';
        },
        (err: any) =>console.log(err)
      );

      this.projectmanagerservice.updateTask(this.task).subscribe(
      //  () => this.router.navigateByUrl('viewTask'),
        (err) =>console.log(err)
      );


    }

    sortByStartDate(){
  
      this.tasks.sort(function(a,b){
        return a.startDate > b.startDate ? -1 : 1
      });
    }
    
    sortByEndDate(){
      
      this.tasks.sort(function(a,b){
        return a.endDate > b.endDate ? -1 : 1
      });
    }
    
    sortByPriority(){
      
      this.tasks.sort(function(a,b){
        return a.priority > b.priority ? -1 : 1
      });
    }
    
    // sortByCompleted(){
      
    //   this.tasks.sort(function(a,b){
    //     return a.completeStatus > b.completeStatus ? 1 : -1
    //   });
    // }

}
