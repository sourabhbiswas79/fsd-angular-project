import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'
// import { IgxSliderModule } from 'igniteui-angular';
import { Ng5SliderModule } from 'ng5-slider';
import { ProjectManagerSvc } from './projectmanager/projectmanager.service';

import { AppComponent } from './app.component';
import { ProjectmanagerComponent } from './projectmanager/projectmanager.component';
import { HeaderComponent } from './projectmanager/header/header.component';
import { AddProjectComponent } from './projectmanager/add-project/add-project.component';
import { AddTaskComponent } from './projectmanager/add-task/add-task.component';
import { AddUserComponent } from './projectmanager/add-user/add-user.component';
import { ViewTaskComponent } from './projectmanager/view-task/view-task.component';
import { AppRoutingModule} from './projectmanager/app-routing/app-routing.module';
import { DatePipe } from '@angular/common';
import { FilterPipe } from './projectmanager/projectmanager.filter-pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProjectmanagerComponent,
    HeaderComponent,
    AddProjectComponent,
    AddTaskComponent,
    AddUserComponent,
    ViewTaskComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    Ng5SliderModule,
    CommonModule
    // IgxSliderModule
  ],
  providers: [ProjectManagerSvc,DatePipe,FilterPipe],
  //exports:[FilterPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
